<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 
IncludeTemplateLangFile(__FILE__); ?>
<div class="container content">			
<? 
	if($arResult['NEED_AUTH'] == "Y")
	{
		?>
			<div class="row">
				<div class="col-md-12">
					<?=GetMessage("NEED_AUTH")?>
				</div>
			</div>
		<?
	}
	elseif(isset($arResult['ITEMS']))
	{
		if(!empty($arResult['ITEMS']))
		{
			foreach($arResult['ITEMS'] as $index => $arItem)
			{
				if($index != 0)
					echo '<hr>';
				?>
					<div class="row product-item">
						<div class="col-md-2">
							<img src="<?=CFile::GetPath($arItem["PICTURE"]);?>">
						</div>
						<div class="col-md-8">
							<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
						</div>
						<div class="col-md-1">
							<a class="favorite-remove" href="#" data-productId="<?=$arItem["ID"]?>" title="<?=GetMessage("REMOVE_FROM_LIST")?>"><?=GetMessage("REMOVE_FROM_LIST")?></a>
						</div>
					</div>
				<?
			}
		} 
		else 
		{
			?>
				<div class="row">
					<div class="col-md-12">
						<?=GetMessage("LIST_EMPTY")?> <a href="/catalog/"><?=GetMessage("TO_CATALOG")?></a>.
					</div>
				</div>
			<? 	
		}
	}	
?>
</div>
