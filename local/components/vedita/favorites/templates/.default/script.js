$(document).ready(function(){
	$('a.favorite-remove').click(function(event){
		var $this = $(this);
		event.preventDefault();
		BX.ajax.runComponentAction
		(
			'vedita:favorites',
			'remove', 
			{ 
				mode: 'class',
				data: 
				{
					iProductId: $(this).attr('data-productId'), 
					iHlBlockId: 4,
				} 
			}
		)
		.then(
			function(response) 
			{
				if (response.status === 'success' && response.data.success === true) {
					$this.closest('.row').remove();
				}
			}
		);
	});
});