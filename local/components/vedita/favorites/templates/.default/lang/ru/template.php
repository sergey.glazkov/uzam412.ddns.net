<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); 

$MESS["LIST_EMPTY"] = "Список отложенных товаров пуст.";
$MESS["TO_CATALOG"] = "Перейти в каталог";
$MESS["REMOVE_FROM_LIST"] = "Удалить из списка";
$MESS["NEED_AUTH"] = "Авторизуйтесь, чтобы использовать функционал отложенных товаров.";