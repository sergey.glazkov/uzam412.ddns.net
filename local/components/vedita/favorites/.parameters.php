<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = [
	"GROUPS" => [],
	"PARAMETERS" => [
		"CACHE_TYPE" => [
			"PARENT" => "BASE",
			"NAME" => "Тип кеширования",
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => "A",
		],
		"CACHE_TIME" => [
			"PARENT" => "BASE",
			"NAME" => "Время кеширования",
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => "86400",
		],
		"HL_BLOCK_ID" => [
			"PARENT" => "BASE",
			"NAME" => "HighloadBlock ID",
			"TYPE" => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT" => "4",
		],
	],
];
?>