<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); 

use Bitrix\Main\Engine\Contract\Controllerable;

class FavoritesComponent extends CBitrixComponent implements Controllerable
{
	public function onPrepareComponentParams($arParams)
	{
		$result = [
			"CACHE_TYPE" => $arParams["CACHE_TYPE"], 
			"CACHE_TIME" => isset($arParams["CACHE_TIME"]) ? (int)$arParams["CACHE_TIME"] : 86400, 
			"HL_BLOCK_ID" => isset($arParams["HL_BLOCK_ID"]) ? (int)$arParams["HL_BLOCK_ID"] : 4,
		];
		
		return $result;
	}
	
	public function executeComponent()
    {
		try
		{
			global $USER;
			if($USER->IsAuthorized())
			{
				if($this->startResultCache(false, $USER->GetID()))
				{		
					$this->arResult['ITEMS'] = $this->getList($USER->GetID());
					if(empty($this->arResult['ITEMS']))
						$this->AbortResultCache();
					$this->IncludeComponentTemplate();
				}
			}
			else
			{
				$this->arResult["NEED_AUTH"] = "Y";
				$this->IncludeComponentTemplate();
			}
		}
		catch (SystemException $e)
		{
			ShowError($e->getMessage());
		}
	}
	
	public function getList($iUserId)
	{
		CModule::IncludeModule("highloadblock"); 
		CModule::IncludeModule("iblock");

		$entity_data_class = $this->getHighloadBlockClass($this->arParams["HL_BLOCK_ID"]);
		
		$arFavoriteProducts = [];
		
		$rsData = $entity_data_class::getList([
		   "select" => ["UF_PRODUCT_ID"],
		   "order" => ["ID" => "ASC"],
		   "filter" => ["UF_USER_ID" => $iUserId]
		]);
		while($arData = $rsData->Fetch())
		{
			$rsProduct = CIBlockElement::GetByID($arData["UF_PRODUCT_ID"]);
			if($arProduct = $rsProduct->GetNext())
			{
				$arFavoriteProducts[] = [
					"ID" => $arProduct["ID"], 
					"NAME" => $arProduct["NAME"], 
					"PICTURE" => $arProduct["PREVIEW_PICTURE"] ?: $arProduct["DETAIL_PICTURE"], 
					"DETAIL_PAGE_URL" => $arProduct["DETAIL_PAGE_URL"],
				];
			}
		}
		return $arFavoriteProducts;
	}
	
	public function getHighloadBlockClass($iHLBlockId)
	{
		$hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById($iHLBlockId)->fetch(); 
		$entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock); 
		$entity_data_class = $entity->getDataClass(); 
		return $entity_data_class;
	}
	
	public function configureActions()
	{
		return [
			'remove' => [ 
				'prefilters' => [],
			],
			'add' => [
				'prefilters' => [],
			],
			'check' => [
				'prefilters' => [],
			],
		];
	}
	public function addAction($iProductId, $iHlBlockId)
	{
		global $USER;
		$iUserId = $USER->IsAuthorized() ? $USER->GetID() : 0;
		
		if(!$iUserId)
			return ["success" => false];
		
		CModule::IncludeModule("highloadblock"); 
		$entity_data_class = $this->getHighloadBlockClass($iHlBlockId);
		
		$rsData = $entity_data_class::getList([
		   "select" => ["ID"],
		   "order" => ["ID" => "ASC"],
		   "filter" => ["UF_USER_ID" => $iUserId, "UF_PRODUCT_ID" => $iProductId], 
		]);
		if($arData = $rsData->Fetch())
			return ["success" => true];
		
		$arData = [
			"UF_PRODUCT_ID" => $iProductId, 
			"UF_USER_ID" => $iUserId,
		];
		$entity_data_class::add($arData);
		
		CBitrixComponent::clearComponentCache('vedita:favorites');

		return ["success" => true];
	}
	
	public function removeAction($iProductId, $iHlBlockId)
	{
		global $USER;
		$iUserId = $USER->IsAuthorized() ? $USER->GetID() : 0;
		if(!$iUserId)
			return ["success" => false];

		CModule::IncludeModule("highloadblock"); 
		$entity_data_class = $this->getHighloadBlockClass($iHlBlockId);
		
		$rsData = $entity_data_class::getList([
		   "select" => ["ID"],
		   "order" => ["ID" => "ASC"],
		   "filter" => ["UF_USER_ID" => $iUserId, "UF_PRODUCT_ID" => $iProductId], 
		]);
		if($arData = $rsData->Fetch())
		{
			$entity_data_class::Delete($arData["ID"]);
			CBitrixComponent::clearComponentCache('vedita:favorites');
		}
		
		return ["success" => true];
	}
	
	public function checkAction($iProductId, $iHlBlockId)
	{
		global $USER;
		$iUserId = $USER->IsAuthorized() ? $USER->GetID() : 0;
		if(!$iUserId)
			return ["success" => false];

		CModule::IncludeModule("highloadblock"); 
		$entity_data_class = $this->getHighloadBlockClass($iHlBlockId);
		
		$rsData = $entity_data_class::getList([
		   "select" => ["ID"],
		   "order" => ["ID" => "ASC"],
		   "filter" => ["UF_USER_ID" => $iUserId, "UF_PRODUCT_ID" => $iProductId], 
		]);
		if($arData = $rsData->Fetch())
			return ["productExist" => true];

		return ["productExist" => false];
	}
}	